# MN32
A (simple) config tool for the Mionix Naos 3200 mouse for linux

# Features
*  Enable/Disable LEDs (configurable per DPI step)
*  Set DPI values (800/1600/3200)
*  Set polling rate (1000/500/250/125 Hz)
*  Custom button assignments for all 7 mouse buttons
   * [x] Left/Right click
   * [x] Scroll click
   * [x] Back/Forward
   * [x] DPI Up/Down/Roll
   * [x] Scroll Up/Down
   * [x] Disable button
   * [X] Map single keyboard key to a mouse button
   * [ ] TODO: Macros
*   Settings file (`XDG_CONFIG_HOME/mn32.ini`)

# Packages
Archlinux AUR: https://aur.archlinux.org/packages/mn32-git/

# Manual installation / Build instructions
- Dependencies:
    - Build essentials of your distro (gcc, make, ...)
    - libusb
    - Qt 5 (qmake, qt-base)
- Clone the repository
- Run `qmake mn32/mn32.pro` to create the Makefile
- Run `make` to build the tool
- Run `make install` to install the binary to `/usr/bin/mn32`
- Either run the tool as `root` or add a udev rule `SUBSYSTEM=="usb", ATTRS{idVendor}=="22d4", ATTRS{idProduct}=="1100", MODE="0660", TAG+="uaccess"`
