#pragma once
#include <QMainWindow>
#include <QMessageBox>
#include <QStandardPaths>
#include <QComboBox>
#include <QAction>
#include <QInputDialog>

#include "controller.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

namespace mn32 {
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    void applySettings();
    void saveToCfgFile();

    void BtnAssgnToMouse(int, Mouse::BtnAssgnment);

private:
    Ui::MainWindow *ui;

    void readMouse();

    //Mapping ui elements to arrays
    QComboBox* _comboPollRate = nullptr;
    std::array<QCheckBox*, LED_AMOUNT> _comboLED{};
    std::array<QComboBox*, DPI_AMOUNT> _comboDPI{};
    std::array<QComboBox*, BTN_AMOUNT> _comboButtonAssgn{};
};
}
