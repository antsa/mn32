#include "mainwindow.h"

#include <QApplication>

using namespace mn32;

auto main(int argc, char *argv[]) -> int
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return QApplication::exec();
}
