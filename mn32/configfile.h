#pragma once
#include "mouse.h"
#include <QSettings>
#include <QDir>
#include <memory>

namespace mn32 {
class ConfigFile
{
    public:
        ConfigFile(const QString& filename = "mn32.ini");

        auto readFromFile() const -> std::unique_ptr<Mouse>;
        auto saveToFile(const std::unique_ptr<Mouse>&) -> void;

    private:
        QSettings _file;
};
}
