#include "usb.h"

using namespace mn32;

USB::USB() = default;
USB::~USB() = default;

auto USB::applySettings(const std::unique_ptr<Mouse> &mouse) -> int
{
    int status = libusb_init(nullptr);
    if(status != LIBUSB_SUCCESS) { return status; }
    libusb_set_option(nullptr, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_ERROR);

    _devh = libusb_open_device_with_vid_pid(nullptr, DEVICE_VID, DEVICE_PID);
    if(_devh == nullptr) { return LIBUSB_ERROR_NO_DEVICE; }

    libusb_set_auto_detach_kernel_driver(_devh, 1);
    status = libusb_claim_interface(_devh, 0);
    if(status != LIBUSB_SUCCESS) { return status; }

    try {
        applyBtnAssgn(mouse->getBtnAssgnments(), mouse->getKeyAssgnments());
        applyLED(mouse->getLEDs());
        applyPollRate(mouse->getPollRate());
        applyDPI(mouse->getDPIs());
        // Apply request on mouse
        sendToMouse(mn32::settingRequest{REQ_PREFIX, REQ_APPLY, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
    }  catch (int e) {
        status = e;
    }

    libusb_release_interface(_devh, 0);
    libusb_close(_devh);
    _devh = nullptr;
    libusb_exit(nullptr);
    return status;
}

auto USB::applyLED(const std::array<bool, LED_AMOUNT> &led) const -> void
{
    mn32::settingRequest tmp{ { REQ_PREFIX, REQ_LED, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    tmp.at(2) = led.at(0) * 0x01;
    tmp.at(3) = led.at(1) * 0x02;
    tmp.at(4) = led.at(2) * 0x03;
    sendToMouse(tmp);
}

auto USB::applyDPI(const std::array<Mouse::DpiRate, DPI_AMOUNT> &dpi) const -> void
{
    // DPI 1 & DPI 2
    mn32::settingRequest tmp1{ { REQ_PREFIX, REQ_DPI12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    tmp1.at(2) = 0x03 << dpi.at(0);
    tmp1.at(3) = 0x20 << dpi.at(0);
    tmp1.at(4) = 0x03 << dpi.at(1);
    tmp1.at(5) = 0x20 << dpi.at(1);

    // DPI 3
    mn32::settingRequest tmp2{ { REQ_PREFIX, REQ_DPI3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    tmp2.at(2) = 0x03 << dpi.at(2);
    tmp2.at(3) = 0x20 << dpi.at(2);

    sendToMouse(tmp1);
    sendToMouse(tmp2);
}

auto USB::applyPollRate(const Mouse::PollRate polRate) const -> void
{
    mn32::settingRequest tmp{ { REQ_PREFIX, REQ_POLLRATE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    tmp.at(2) = polRate + 1;

    sendToMouse(tmp);
}

auto USB::applyBtnAssgn(const std::array<Mouse::BtnAssgnment, BTN_AMOUNT> &btnAssgn, const std::array<Mouse::UsbHidKey, BTN_AMOUNT> &keyAssgn) const -> void
{
    // Buttons (1)-(5)
    mn32::settingRequest tmpa1{ { REQ_PREFIX, REQ_BTNA1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    mn32::settingRequest tmpa2{ { REQ_PREFIX, REQ_BTNA2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    for(int i = 0; i < 5; i++)
    {
        tmpa1.at(i+2) = _buttonCodes.at(btnAssgn.at(i)).first;
        if(btnAssgn.at(i) == Mouse::Btn_KbdKey) {
            tmpa2.at(i+2) = keyAssgn.at(i);
        } else {
            tmpa2.at(i+2) = _buttonCodes.at(btnAssgn.at(i)).second;
        }
    }

    // Buttons (6)-(7)
    mn32::settingRequest tmpb1{ { REQ_PREFIX, REQ_BTNB1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    mn32::settingRequest tmpb2{ { REQ_PREFIX, REQ_BTNB2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
    for(int i = 0; i < 2; i++)
    {
        tmpb1.at(i+2) = _buttonCodes.at(btnAssgn.at(i+5)).first;
        if(btnAssgn.at(i+5) == Mouse::Btn_KbdKey) {
            tmpb2.at(i+2) = keyAssgn.at(i+5);
        } else {
            tmpb2.at(i+2) = _buttonCodes.at(btnAssgn.at(i+5)).second;
        }
    }

    sendToMouse(tmpa1);
    sendToMouse(tmpb1);
    sendToMouse(tmpa2);
    sendToMouse(tmpb2);
}

auto USB::sendToMouse(const mn32::settingRequest &send) const -> void
{
    int status = libusb_reset_device(_devh);
    if(status != LIBUSB_SUCCESS) { throw int(status); }

    mn32::settingRequest buf = send;
    status = libusb_control_transfer(_devh, LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE , LIBUSB_REQUEST_SET_CONFIGURATION, XFER_SETUP_PKG, 0, buf.data(), buf.size(), XFER_TIMEOUT);
    if(status != sizeof(buf)) { throw int(status); }

    std::this_thread::sleep_for(std::chrono::milliseconds(XFER_WAIT));

    buf = {};
    status = libusb_control_transfer(_devh, LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE, LIBUSB_REQUEST_CLEAR_FEATURE, XFER_SETUP_PKG, 0, buf.data(), buf.size(), XFER_TIMEOUT);
    if(status != sizeof(buf)) { throw int(status); }
}

