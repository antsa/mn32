QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    configfile.cpp \
    controller.cpp \
    main.cpp \
    mainwindow.cpp \
    mouse.cpp \
    usb.cpp

HEADERS += \
    configfile.h \
    controller.h \
    mainwindow.h \
    mouse.h \
    usb.h

FORMS += \
    mainwindow.ui

INCLUDEPATH += /usr/include/libusb-1.0/
LIBS += -lusb-1.0

target.path = /usr/bin
INSTALLS += target
