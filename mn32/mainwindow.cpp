#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace mn32;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Initialize ui elements
    _comboLED = {ui->ledDPI1CheckBox, ui->ledDPI2CheckBox, ui->ledDPI3CheckBox};
    _comboDPI = {ui->dPI1ComboBox, ui->dPI2ComboBox, ui->dPI3ComboBox};
    _comboPollRate = ui->pollingRateComboBox;
    _comboButtonAssgn = {ui->leftClickComboBox, ui->rightClickComboBox, ui->scrollClickComboBox, ui->backComboBox, ui->forwardComboBox, ui->upComboBox, ui->downComboBox};
    for(int i=0; i<DPI_AMOUNT; i++) {
        _comboDPI.at(i)->addItems(Mouse::dpiRateOptions);
    }
    _comboPollRate->addItems(Mouse::pollRateOptions);
    for(int i=0; i<BTN_AMOUNT; i++) {
        _comboButtonAssgn.at(i)->addItems(Mouse::btnAssgnmentOptions);
    }

    // Read mouse object for ui
    readMouse();

    // Setup slot/signals for ui/backend communication
    for(int i=0; i<LED_AMOUNT; i++) {
        QObject::connect(_comboLED.at(i), &QCheckBox::stateChanged, [i](bool state) {Controller::getInstance().getMouse()->setLED(i, state);} );
    }
    for(int i=0; i<DPI_AMOUNT; i++) {
        QObject::connect(_comboDPI.at(i), QOverload<int>::of(&QComboBox::currentIndexChanged), [i](int value) {Controller::getInstance().getMouse()->setDPI(i, (Mouse::DpiRate) value);} );
    }
    QObject::connect(_comboPollRate, QOverload<int>::of(&QComboBox::currentIndexChanged), [](int value) {Controller::getInstance().getMouse()->setPollRate((Mouse::PollRate) value);} );
    for(int i=0; i<BTN_AMOUNT; i++) {
        QObject::connect(_comboButtonAssgn.at(i), QOverload<int>::of(&QComboBox::activated), [this, i](int value) {BtnAssgnToMouse(i, (Mouse::BtnAssgnment) value);});
    }
    QObject::connect(ui->applypushButton, &QAbstractButton::pressed, this, &MainWindow::applySettings);
    QObject::connect(ui->actionSave, &QAction::triggered, this, &MainWindow::saveToCfgFile);
}

MainWindow::~MainWindow()
{
    for(int i=0; i<LED_AMOUNT; i++) {
        delete _comboLED.at(i);
    }
    for(int i=0; i<DPI_AMOUNT; i++) {
        delete _comboDPI.at(i);
    }
    delete _comboPollRate;
    for(int i=0; i<BTN_AMOUNT; i++) {
        delete _comboButtonAssgn.at(i);
    }
    Controller::releaseInstance();
    delete ui;
}

void MainWindow::readMouse() {
    for(int i = 0; i<LED_AMOUNT; i++) {
        _comboLED.at(i)->setCheckState(Controller::getInstance().getMouse()->getLEDs().at(i) ? Qt::Checked : Qt::Unchecked);
    }

    for(int i = 0; i<DPI_AMOUNT; i++) {
        _comboDPI.at(i)->setCurrentIndex(Controller::getInstance().getMouse()->getDPIs().at(i));
    }

    _comboPollRate->setCurrentIndex(Controller::getInstance().getMouse()->getPollRate());

    for(int i = 0; i<BTN_AMOUNT; i++) {
        if(Controller::getInstance().getMouse()->getBtnAssgnments().at(i) == Mouse::Btn_KbdKey)
        {
            _comboButtonAssgn.at(i)->setItemText(Mouse::Btn_KbdKey, "Keyboard Key (" + Mouse::kbdKeyOptions.at(Controller::getInstance().getMouse()->getKeyAssgnments().at(i) - 4) + ")");
        }
        _comboButtonAssgn.at(i)->setCurrentIndex(Controller::getInstance().getMouse()->getBtnAssgnments().at(i));
    }
}

void MainWindow::applySettings()
{
    int i = Controller::getInstance().sendSettingsToMouse();
    if(i < 0) {
        QMessageBox::critical(this, "USB Error!", "libusb error code: " + QString::number(i), QMessageBox::Ok);
    } else {
        QMessageBox::information(this, "Settings applied!", "The settings were sent to the mouse", QMessageBox::Ok);
    }
    saveToCfgFile();
}

void MainWindow::saveToCfgFile()
{
    Controller::getInstance().saveSettingsToFile();
}

void MainWindow::BtnAssgnToMouse(int i, Mouse::BtnAssgnment value)
{
    _comboButtonAssgn.at(i)->setItemText(Mouse::Btn_KbdKey, "Keyboard Key");
    if (value == Mouse::Btn_KbdKey) {
        bool ok;
        QString item = QInputDialog::getItem(this, "Select Key", "Select a keyboad key (US Keyboard Layout!):", Mouse::kbdKeyOptions, 0, false, &ok);
        if(ok) {
            Controller::getInstance().getMouse()->setKeyAssgnment(i, (Mouse::UsbHidKey) (Mouse::kbdKeyOptions.indexOf(item) + 4));
            _comboButtonAssgn.at(i)->setItemText(Mouse::Btn_KbdKey, "Keyboard Key (" + item + ")");
        } else {
            _comboButtonAssgn.at(i)->setCurrentIndex(Mouse::Btn_Disabled);
            Controller::getInstance().getMouse()->setBtnAssgnment(i, Mouse::Btn_Disabled);
            return;
        }
    }
    Controller::getInstance().getMouse()->setBtnAssgnment(i, value);
}
