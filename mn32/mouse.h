// Mouse object for handling mouse settings
#pragma once
#include <array>
#include <QStringList>

// Amount of leds, dpi steps and buttons on mouse
#define BTN_AMOUNT 7
#define LED_AMOUNT 3
#define DPI_AMOUNT 3

namespace mn32 {
class Mouse{
public:
    // Available settings
    enum DpiRate {  // DPI rates
        Dpi_800 = 0,
        Dpi_1600,
        Dpi_3200
    };
    enum PollRate { // Polling rates
        Poll_1000 = 0,
        Poll_500,
        Poll_250,
        Poll_125
    };
    enum BtnAssgnment {    // Button assignments
        Btn_LeftClick = 0,  // Left click
        Btn_RightClick,     // Right click
        Btn_ScrollClick,    // Scroll click
        Btn_Backwards,      // Backwards
        Btn_Forwards,       // Forwards
        Btn_DpiUp,          // DPI up
        Btn_DpiDown,        // DPI down
        Btn_DpiRoll,        // DPI roll
        Btn_ScrollUp,       // Scroll uo
        Btn_ScrollDown,     // Scroll down
        Btn_Disabled,       // Disable button
        Btn_KbdKey          // Keyboard key (usb hid keyboard scan code)
    };
    enum UsbHidKey {   // USB HID keyboard scan codes
        Key_A = 0x04, // a&A
        Key_B,        // b&B
        Key_C,        // c&C
        Key_D,        // d&D
        Key_E,        // e&E
        Key_F,        // f&F
        Key_G,        // g&G
        Key_H,        // h&H
        Key_I,        // i&I
        Key_J,        // j&J
        Key_K,        // k&K
        Key_L,        // l&L
        Key_M,        // m&M
        Key_N,        // n&N
        Key_O,        // o&O
        Key_P,        // p&P
        Key_Q,        // q&Q
        Key_R,        // r&R
        Key_S,        // s&S
        Key_T,        // t&T
        Key_U,        // u&U
        Key_V,        // v&V
        Key_W,        // w&W
        Key_X,        // x&X
        Key_Y,        // y&Y
        Key_Z,        // z&Z
        Key_1,        // 1&!
        Key_2,        // 2&@
        Key_3,        // 3&#
        Key_4,        // 4&$
        Key_5,        // 5&%
        Key_6,        // 6&^
        Key_7,        // 7&&
        Key_8,        // 8&*
        Key_9,        // 9&(
        Key_0         // 0&)
    };
    // Ui descriptions
    static const QStringList dpiRateOptions;
    static const QStringList pollRateOptions;
    static const QStringList btnAssgnmentOptions;
    static const QStringList kbdKeyOptions;

    Mouse();
    ~Mouse();
    auto setLED(int, bool) -> void;
    auto setDPI(int, DpiRate) -> void;
    auto setPollRate(PollRate) -> void;
    auto setBtnAssgnment(int, BtnAssgnment) -> void;
    auto setKeyAssgnment(int, UsbHidKey) -> void;

    auto getLEDs() const -> const std::array<bool, LED_AMOUNT>&;
    auto getDPIs() const -> const std::array<DpiRate, DPI_AMOUNT>&;
    auto getPollRate() const -> PollRate;
    auto getBtnAssgnments() const -> const std::array<BtnAssgnment, BTN_AMOUNT>&;
    auto getKeyAssgnments() const -> const std::array<UsbHidKey, BTN_AMOUNT>&;

private:
    std::array<bool, LED_AMOUNT> _ledEffects{ { false, false, false } };
    std::array<DpiRate, DPI_AMOUNT> _dpi{ { Dpi_800, Dpi_800, Dpi_800 } };
    PollRate _pollingRate = Poll_1000;
    std::array<BtnAssgnment, BTN_AMOUNT> _buttonassgn{ { Btn_LeftClick, Btn_RightClick, Btn_ScrollClick, Btn_Backwards, Btn_Forwards, Btn_DpiUp, Btn_DpiDown } };
    std::array<UsbHidKey, BTN_AMOUNT> _keymapassgn{};
};
}
