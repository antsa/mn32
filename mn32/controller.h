#pragma once

#include <memory>

#include "mouse.h"
#include "configfile.h"
#include "usb.h"

namespace mn32 {
class Controller
{
    public:
         static auto getInstance() -> Controller&;
         static auto releaseInstance() -> void;

         auto saveSettingsToFile() -> void;
         auto sendSettingsToMouse() -> int;

         auto getMouse() -> std::unique_ptr<Mouse>&;

    private:
        Controller();
        ~Controller();

        static Controller* _instance;

        std::unique_ptr<Mouse> _mouse = nullptr;
        std::unique_ptr<ConfigFile> _cfgfile = nullptr;
        USB _usbInterface;
};
}
