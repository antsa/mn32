#include "configfile.h"

using namespace mn32;

ConfigFile::ConfigFile(const QString& filename) : _file(filename, QSettings::IniFormat)
{
}

auto ConfigFile::readFromFile() const -> std::unique_ptr<Mouse>
{
    auto tmp = std::make_unique<Mouse>();

    // Read LED settings
    for(int i = 0; i < LED_AMOUNT; i++)
    {
        tmp->setLED(i, _file.value("ledEnabled" + QString::number(i)).toBool());
    }
    // Read DPI steps
    for(int i = 0; i < DPI_AMOUNT; i++)
    {
        tmp->setDPI(i, (Mouse::DpiRate) _file.value("dpiRate" + QString::number(i)).toInt());
    }
    // Read polling rate
    tmp->setPollRate((Mouse::PollRate) _file.value("pollRate").toInt());
    // Read Button Assignments
    for(int i = 0; i < BTN_AMOUNT; i++) {
        tmp->setBtnAssgnment(i, (Mouse::BtnAssgnment) _file.value("btnAssgn" + QString::number(i), i).toInt());
    }
    // Read Key mapping
    for(int i = 0; i < BTN_AMOUNT; i++) {
        int value = _file.value("keyAssgn" + QString::number(i), 4).toInt();
        tmp->setKeyAssgnment(i, (Mouse::UsbHidKey) ((value < 4) ? 4 : value));
    }
    return tmp;
}

auto ConfigFile::saveToFile(const std::unique_ptr<Mouse> &mouse) -> void
{
    // Save LED settings
    for(int i = 0; i < LED_AMOUNT; i++)
    {
        _file.setValue("ledEnabled" + QString::number(i), mouse->getLEDs().at(i));
    }
    // Save DPI steps
    for(int i = 0; i < DPI_AMOUNT; i++)
    {
        _file.setValue("dpiRate" + QString::number(i),  mouse->getDPIs().at(i));
    }
    // Save polling rate
    _file.setValue("pollRate", mouse->getPollRate());
    // Save Button Assignments
    for(int i = 0; i < BTN_AMOUNT; i++) {
        _file.setValue("btnAssgn" + QString::number(i), mouse->getBtnAssgnments().at(i));
    }
    // Save Key Assignments
    for(int i = 0; i < BTN_AMOUNT; i++) {
        _file.setValue("keyAssgn" + QString::number(i), mouse->getKeyAssgnments().at(i));
    }
}
