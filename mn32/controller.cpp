#include "controller.h"

using namespace mn32;

Controller* Controller::_instance = nullptr;

Controller::Controller()
    : _cfgfile(new ConfigFile())
{
    // Read config file
    _mouse = _cfgfile->readFromFile();
}

Controller::~Controller() = default;

auto Controller::getInstance() -> Controller&
{
    if (_instance == nullptr)
    {
        _instance = new Controller();
    }
    return *_instance;
}

auto Controller::releaseInstance() -> void
{
    if (Controller::_instance != nullptr)
    {
        delete Controller::_instance;
        Controller::_instance = nullptr;
    }
}

auto Controller::saveSettingsToFile() -> void
{
    _cfgfile->saveToFile(_mouse);
}

auto Controller::sendSettingsToMouse() -> int
{
    return _usbInterface.applySettings(_mouse);
}

auto Controller::getMouse() -> std::unique_ptr<Mouse>&
{
    return _mouse;
}
