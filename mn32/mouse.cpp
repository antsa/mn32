#include "mouse.h"

using namespace mn32;

const QStringList Mouse::dpiRateOptions {
    "800",
    "1600",
    "3200"
};
const QStringList Mouse::pollRateOptions {
    "1000",
    "500",
    "250",
    "125"
};
const QStringList Mouse::btnAssgnmentOptions {
    "Left click",
    "Right click",
    "Scroll click",
    "Backwards",
    "Forwards",
    "DPI up",
    "DPI down",
    "DPI roll",
    "Scroll up",
    "Scroll down",
    "Disable button",
    "Keyboard key"
};
const QStringList Mouse::kbdKeyOptions {
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "1 and !",
    "2 and @",
    "3 and #",
    "4 and $",
    "5 and %",
    "6 and ^",
    "7 and &",
    "8 and *",
    "9 and (",
    "0 and )"
};

Mouse::Mouse() = default;
Mouse::~Mouse() = default;

auto Mouse::setLED(int i, bool value) -> void
{
    _ledEffects.at(i) = value;
}

auto Mouse::setDPI(int i, DpiRate value) -> void
{
    _dpi.at(i) = value;
}

auto Mouse::setPollRate(PollRate pollingRate) -> void
{
    _pollingRate = pollingRate;
}

auto Mouse::setBtnAssgnment(int i, BtnAssgnment value) -> void
{
    _buttonassgn.at(i) = value;
}

auto Mouse::setKeyAssgnment(int i, UsbHidKey value) -> void
{
    _keymapassgn.at(i) = value;
}

auto Mouse::getLEDs() const -> const std::array<bool, LED_AMOUNT>&
{
    return _ledEffects;
}

auto Mouse::getDPIs() const -> const std::array<DpiRate, DPI_AMOUNT>&
{
    return _dpi;
}
auto Mouse::getPollRate() const -> PollRate
{
    return _pollingRate;
}

auto Mouse::getBtnAssgnments() const -> const std::array<BtnAssgnment, BTN_AMOUNT>&
{
    return _buttonassgn;
}

auto Mouse::getKeyAssgnments() const -> const std::array<UsbHidKey, BTN_AMOUNT>&
{
    return _keymapassgn;
}
