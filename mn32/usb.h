#pragma once

#include <libusb-1.0/libusb.h>
#include "mouse.h"
#include <chrono>
#include <thread>
#include <array>

// Device IDs (Vendor & Product)
#define DEVICE_VID 0x22d4
#define DEVICE_PID 0x1100

// Delay between usb transfer (ms)
#define XFER_WAIT 50
// Request timeout (ms)
#define XFER_TIMEOUT 50
// Value for setup package
#define XFER_SETUP_PKG 0x0303

// Size of request
#define REQ_SIZE 8

// Request prefixes
#define REQ_PREFIX      0x03    // Prefix for all strings
#define REQ_APPLY       0xcb    // Apply settings request
#define REQ_LED         0xcd    // LED settings request
#define REQ_DPI12       0xc0    // DPI1&2 settings request
#define REQ_DPI3        0xc1    // DPI3 settings request
#define REQ_POLLRATE    0xce    // Polling rate setting request
#define REQ_BTNA1       0xc7    // (1) Buttons 1-5 settings request
#define REQ_BTNA2       0xc9    // (2)
#define REQ_BTNB1       0xc8    // (1) Buttons 6-7 settings request
#define REQ_BTNB2       0xca    // (2)

namespace mn32 {
typedef std::array<unsigned char, REQ_SIZE> settingRequest;
class USB
{
    private:
        libusb_device_handle* _devh;

        auto applyLED(const std::array<bool, LED_AMOUNT>&) const -> void;
        auto applyDPI(const std::array<Mouse::DpiRate, DPI_AMOUNT>&) const -> void;
        auto applyPollRate(const Mouse::PollRate) const -> void;
        auto applyBtnAssgn(const std::array<Mouse::BtnAssgnment, BTN_AMOUNT>&, const std::array<Mouse::UsbHidKey, BTN_AMOUNT>&) const -> void;

        auto sendToMouse(const std::array<unsigned char, REQ_SIZE>&) const -> void;

        const std::array<std::pair<unsigned char, unsigned char>, 12> _buttonCodes = {
            std::make_pair<unsigned char, unsigned char>(0x00, 0x01), // Left click
            std::make_pair<unsigned char, unsigned char>(0x00, 0x02), // Right click
            std::make_pair<unsigned char, unsigned char>(0x00, 0x04), // Scroll click
            std::make_pair<unsigned char, unsigned char>(0x00, 0x08), // Backward
            std::make_pair<unsigned char, unsigned char>(0x00, 0x10), // Forward
            std::make_pair<unsigned char, unsigned char>(0x04, 0x01), // DPI up
            std::make_pair<unsigned char, unsigned char>(0x04, 0x02), // DPI down
            std::make_pair<unsigned char, unsigned char>(0x04, 0x00), // DPI roll
            std::make_pair<unsigned char, unsigned char>(0x03, 0x01), // Scroll up
            std::make_pair<unsigned char, unsigned char>(0x03, 0xff), // Scroll down
            std::make_pair<unsigned char, unsigned char>(0x05, 0x00), // Disable button
            std::make_pair<unsigned char, unsigned char>(0x02, 0x00)  // Keyboard key
        };

    public:
        USB();
        ~USB();

        auto applySettings(const std::unique_ptr<Mouse>&) -> int;
};
}
